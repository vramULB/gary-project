package View;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.text.Font;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PlayerInfoLabel extends Label {

    private final static String FONT_PATH = "src/model/ressources/kenvector_future.ttf";
    private final static String BACKGROUND_IMAGE = "model/ressources/grey_panel.png";
    private final static String LABEL_MONEY = "Money";
    private final static String LABEL_SCORE = "Score";
    private final static int WIDTH = 250;
    private final static int HEIGHT = 100;
    private int money;
    private int score;

    public PlayerInfoLabel(int money, int score) {
        this.money = money;
        this.score = score;
        setPrefWidth(WIDTH);
        setPrefHeight(HEIGHT);
        BackgroundImage image = new BackgroundImage(new Image(BACKGROUND_IMAGE, WIDTH, HEIGHT, false, true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, null);

        setBackground(new Background(image));

        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(10, 10, 10, 10));
        setLabelFont();
        updateNewText();
    }

    public void setMoney(int money) {
        this.money = money;
        updateNewText();
    }

    public void setScore(int score) {
        this.score = score;
        updateNewText();
    }

    private void updateNewText() {
        setText(LABEL_MONEY + " : " + money + '\n' + LABEL_SCORE + " : " + score);
    }

    private void setLabelFont() {
        try {
            setFont(Font.loadFont(new FileInputStream(new File(FONT_PATH)), 15));
        } catch (FileNotFoundException e) {
            setFont(Font.font("Verdana", 15));
        }
    }

}
