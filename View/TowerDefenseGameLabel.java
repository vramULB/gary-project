package View;

import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;

import java.awt.*;
import java.util.Collection;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.text.Font;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class TowerDefenseGameLabel extends Label{

    private final static String FONT_PATH = "src/model/ressources/kenvector_future.ttf";
    private final static String BACKGROUND_IMAGE = "View/ressources/red_button12.png";

    public TowerDefenseGameLabel(String text) {
        setPrefWidth(250);
        setPrefHeight(152);

        BackgroundImage image = new BackgroundImage(new Image(BACKGROUND_IMAGE, 250, 152, false, true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, null);

        setBackground(new Background(image));

        setAlignment(Pos.CENTER);
        setPadding(new Insets(10, 10, 10, 10));
        setLabelFont();
        setText(text);

    }

    private void setLabelFont() {
        try {
            setFont(Font.loadFont(new FileInputStream(new File(FONT_PATH)), 15));
        } catch (FileNotFoundException e) {
            setFont(Font.font("Verdana", 15));
        }
    }
	
}
