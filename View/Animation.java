package View;

import Model.Facade;
import Model.Game;
import Model.actors.Actors;
import Model.actors.Pnj;
import Model.actors.Projectil;
import Model.actors.Tower;
import javafx.animation.AnimationTimer;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Animation extends AnimationTimer {
    private int nbPnjWave;
    private int maxNbPnjWave;
    private Facade facade;
    GameViewManager gameViewManager;

    public Animation(int maxNbPnjWave, Facade facade, GameViewManager gameViewManager) {
        this.nbPnjWave=0;
        this.maxNbPnjWave = maxNbPnjWave;
        this.facade = facade;
        this.gameViewManager = gameViewManager;
    }

    @Override
    public void handle(long l) {

        if (facade.getPnjs().isEmpty()) {
            facade.stopCurrentWave();
            ++nbPnjWave;
            if (nbPnjWave > maxNbPnjWave) {
                gameViewManager.removeCells();
                gameViewManager.removeTowers();
                gameViewManager.removeProjectils(facade.getProjectils());
                facade.nextMap();
                gameViewManager.createCells();
                nbPnjWave = 0;
                if (facade.isEndOfGame()) {
                    gameViewManager.removeTowers();
                    gameViewManager.removePnjs();
                    System.out.println("GAME FINISH !! ");
                    gameViewManager.stopAnimation();
                }
            }
            facade.createWave(nbPnjWave, 1);
        }
        synchronized (Game.keyPnj) {
            for (Pnj pnj : facade.getPnjs()) {
                ImageView iView = (ImageView) pnj.getImageOfTheActor();
                if (pnj.isAlive()) {
                    gameViewManager.addObject(iView);
                    //Set the new X position and Y
                    if (gameViewManager.isAtEndOfMap(pnj)) {
                        facade.modifyScorePlayer(pnj);
                        pnj.setAlive(false);
                        gameViewManager.removeObject(iView);
                    } else {
                        pnj.runThread();
                        //synchronized (Game.keyPnj) {
                        //Move in board
                        facade.getPnjs().forEach(Actors::updatePlaceOfTheActor);
                        // }
                    }
                } else if (!pnj.isAlive()) {
                    facade.modifyMoneyPnjPlayer(pnj);
                }
            }

            //DELETE PNJ
            List<Pnj> toDelete = facade.getPnjs().stream()
                    .filter(pnj -> !pnj.isAlive())
                    .collect(Collectors.toList());
            toDelete.forEach(pnj -> {
                //((PnjHostil) pnj).interruptThread();
                gameViewManager.removeObject(pnj.getImageOfTheActor());
            });
            synchronized (Game.keyPnj) {
                facade.getPnjs().removeAll(toDelete);
            }

        }
        //TOWER
        for (Tower t : facade.getTowers()) {
            gameViewManager.addObject(t.getImageOfTheActor());
        }

        //PROJECTILS

        List<Projectil> toDeleteProj = new ArrayList<>();

        synchronized (Game.keyProjectil) {
            for (Projectil p : facade.getProjectils()) {
                gameViewManager.addObject(p.getImageOfTheActor());
                if (p.outOfRadius() || p.hasTouch()) {
                    toDeleteProj.add(p);
                } else {
                    p.updatePlaceOfTheActor();
                }

            }
        }
        gameViewManager.removeProjectils(toDeleteProj);

        gameViewManager.setScorePlayer(facade.getScorePlayer());
        gameViewManager.setMoneyPlayer(facade.getMoneyPlayer());


    }
}
