package View;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ViewManager {

    private static final int HEIGHT = 768;
    private static final int WIDTH = 1024;
    private static final int MENU_BUTTONS_START_X = 100;
    private static final int MENU_BUTTONS_START_Y = 200;

    private AnchorPane mainPane;
    private Scene mainScene;
    private Stage mainStage;

    private TowerDefenseSubscene playSubScene;
    private TowerDefenseSubscene creatorsSubScene;
    private TowerDefenseSubscene helpSubScene;
    private TowerDefenseSubscene scoresSubScene;
    ArrayList<TowerDefenseMenuButton> menuButtons;

    public ViewManager() {
        this.menuButtons = new ArrayList<>();
        this.mainPane = new AnchorPane();
        this.mainScene = new Scene(mainPane, WIDTH, HEIGHT);
        this.mainStage = new Stage();
        this.mainStage.setScene(mainScene);
        createSubScene();
        createButtons();
        createBackGround();
        createLogo();
    }

    private void createSubScene() {
        playSubScene = new TowerDefenseSubscene();
        mainPane.getChildren().add(playSubScene);

        helpSubScene = new TowerDefenseSubscene();
        mainPane.getChildren().add(helpSubScene);

        creatorsSubScene = new TowerDefenseSubscene();
        mainPane.getChildren().add(creatorsSubScene);

        scoresSubScene = new TowerDefenseSubscene();
        mainPane.getChildren().add(scoresSubScene);
    }

    public Stage getMainStage() {
        return mainStage;
    }

    private void addMenuButton(TowerDefenseMenuButton button) {
        button.setLayoutX(MENU_BUTTONS_START_X);
        button.setLayoutY(MENU_BUTTONS_START_Y + menuButtons.size() * 100);
        menuButtons.add(button);
        mainPane.getChildren().add(button);
    }

    //CREATION OF THE BUTTONS OF THE MENU
    private void createButtons() {
        createStartButton();
        createScoresButton();
        createHelpButton();
        createCreatorsButton();
        createExitButton();
    }

    private void createStartButton() {
        TowerDefenseMenuButton startButton = new TowerDefenseMenuButton("PLAY");
        addMenuButton(startButton);

        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                //GameViewManager gameManager = new GameViewManager();
                GameViewManager gameManager = new GameViewManager("GARY");
                mainStage.hide();
                gameManager.launchGame();
            }
        });
    }

    private void createScoresButton() {
        TowerDefenseMenuButton scoreButton = new TowerDefenseMenuButton("SCORES");
        addMenuButton(scoreButton);

        scoreButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                scoresSubScene.moveSubScene();
            }
        });
    }

    private void createHelpButton() {
        TowerDefenseMenuButton helpButton = new TowerDefenseMenuButton("HELP");
        addMenuButton(helpButton);

        helpButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                helpSubScene.moveSubScene();
            }
        });
    }


    private void createCreatorsButton() {
        TowerDefenseMenuButton creatorsButton = new TowerDefenseMenuButton("CREATORS");
        addMenuButton(creatorsButton);

        creatorsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                creatorsSubScene.moveSubScene();
            }
        });
    }

    private void createExitButton() {
        TowerDefenseMenuButton exitButton = new TowerDefenseMenuButton("EXIT");
        addMenuButton(exitButton);

        exitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                mainStage.close();
            }
        });
    }

    // BACKGROUND OF THE MENU
    private void createBackGround() {
        Image backgroundImage = new Image("view/ressources/blue.jpg", 256, 256, false, true);
        BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, null);
        mainPane.setBackground(new Background(background));
    }

    // LOGO FOR THE MENU
    private void createLogo() {
        ImageView logo = new ImageView("view/ressources/logo.png");
        logo.setLayoutX(400);
        logo.setLayoutY(50);
        logo.setFitHeight(150);
        logo.setFitWidth(500);

        logo.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                logo.setEffect(new DropShadow());
            }
        });

        logo.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                logo.setEffect(null);
            }
        });

        mainPane.getChildren().add(logo);
    }

}
