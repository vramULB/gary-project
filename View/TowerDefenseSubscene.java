package View;

import javafx.animation.TranslateTransition;
import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.util.Duration;

public class TowerDefenseSubscene extends SubScene {

    private final static String FONT_PATH = "src/Model/ressources/kenvector_future.ttf";
    private final static String BACKGROUND_IMAGE = "Model/ressources/grey_panel.png";

    private boolean isHidden;

    public TowerDefenseSubscene() {
        super(new AnchorPane(), 600, 400);
        prefWidth(600);
        prefHeight(400);

        BackgroundImage image = new BackgroundImage(new Image(BACKGROUND_IMAGE, 600, 400, false, true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, null);

        AnchorPane root2 = (AnchorPane) this.getRoot();

        root2.setBackground(new Background(image));

        setLayoutY(230);
        setLayoutX(1024);
    }

    public void moveSubScene() {
        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.seconds(0.3));
        transition.setNode(this);

        if (isHidden) {
            transition.setToX(-676);
            isHidden = false;
        } else{
            transition.setToX(0);
            isHidden = true;
        }
        transition.play();
    }
	
}
