package View;

import Controller.*;
import Model.*;
import Model.actors.*;
import Model.dto.TowerInfoDTO;
import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// VOIR VIDEO 10 POUR TIMER
// VOIR VIDEO 12 POUR ENVOYER LES PNJ AVEC UN DELAI

public class GameViewManager {

    private static final int GAME_WIDTH = 1280;
    private static final int GAME_HEIGHT = 900;
    private static final int PLAYER_INIT_SCORE = 1000;
    private static final int PLAYER_INIT_MONEY = 300;
    private static final int HEIGTH_OBJECT = 64;
    private static final int WIDTH_OBJECT = 64;
    private static final int MAX_NB_PNJ_WAVE = 10;

    private AnchorPane gamePane1;
    private Scene gameScene1;
    private Stage gameStage;
    private TowerDefenseGameLabel towerInfoLabel;
    private PlayerInfoLabel playerInfoLabel;

    private boolean isTower1Placable;
    private boolean isTower2Placable;
    private boolean clickOnTowerUpgrade;
    private boolean clickOnSellTower;

    //Synchronize
    public final Object keyChildren = new Object();
    ;

    //Number of Pnj in Wave
    public int nbPnjWave;


    private Facade facade;
    private Animation animation;

    public GameViewManager(String namePlayer) {
        this.facade = new FacadeImpl();
        this.facade.createGame(new Player(namePlayer, PLAYER_INIT_MONEY, PLAYER_INIT_SCORE), 1);
        this.nbPnjWave = 0;
        this.isTower1Placable = false;
        this.isTower2Placable = false;
        this.clickOnTowerUpgrade = false;
        this.clickOnSellTower = false;
        initializeStage();
    }


    private void initializeStage() {
        gamePane1 = new AnchorPane();
        gameScene1 = new Scene(gamePane1, GAME_WIDTH, GAME_HEIGHT);
        gameStage = new Stage();
    }

    void launchGame() {
        createCells();
        createStartWavePnjButton();
        //createMapChooser();
        createInfoLabelTower();
        createTowerButton1();
        createTowerButton2();
        createPlayerInfoLabel2();
        createUpgradeButton();
        createSellButton();
        this.getGamePane1().setOnMouseClicked(new PaneListener(this, this.facade.getCurrentMapArray(), facade));
        this.getGamePane1().setOnMouseMoved(new PaneMoverListener(this));
        gameStage.setScene(gameScene1);
        gameStage.show();
    }


    public void play() {
        this.animation = new Animation(MAX_NB_PNJ_WAVE, this.facade, this);
        animation.start();
    }

    void stopAnimation() {
        this.animation.stop();
    }

    // CREATE REMOVE
    private void createStartWavePnjButton() {
        TowerDefenseMenuButton startWavePnjButton = new TowerDefenseMenuButton("LET'S GOOO");
        this.addObject(startWavePnjButton);
        startWavePnjButton.setLayoutY(700);
        startWavePnjButton.setLayoutX(600);
        startWavePnjButton.setOnMouseClicked(new StartWaveListener(facade.getGame(), this));
    }

    private void createTowerButton1() {
        TowerDefenseMenuButton towerButton1 = new TowerDefenseMenuButton("Tower 1");
        this.addObject(towerButton1);
        towerButton1.setLayoutX(250);
        towerButton1.setLayoutY(650);
        towerButton1.setOnMouseClicked(new Tower1ButtonListener(this));
        towerButton1.setOnMouseEntered(new TowerInfoListener(this, facade.getInfoTowerType1()));
    }

    private void createTowerButton2() {
        TowerDefenseMenuButton towerButton2 = new TowerDefenseMenuButton("Tower 2");
        this.addObject(towerButton2);
        towerButton2.setLayoutX(400);
        towerButton2.setLayoutY(650);
        towerButton2.setOnMouseClicked(new Tower2ButtonListener(this));
        towerButton2.setOnMouseEntered(new TowerInfoListener(this, facade.getInfoTowerType2()));

    }

    private void createUpgradeButton() {
        TowerDefenseMenuButton upgradeButton = new TowerDefenseMenuButton("UPGRADE");
        this.addObject(upgradeButton);
        upgradeButton.setLayoutX(250);
        upgradeButton.setLayoutY(700);
        upgradeButton.setOnMouseClicked(new UpgradeClickListener(facade, this));
    }

    private void createSellButton() {
        TowerDefenseMenuButton sellButton = new TowerDefenseMenuButton("SELL $");
        this.addObject(sellButton);
        sellButton.setLayoutX(400);
        sellButton.setLayoutY(700);
        sellButton.setOnMouseClicked(new SellTowerListener(facade, this));
    }


    private void createMapChooser() {
        TowerDefenseMenuButton mapChooser = new TowerDefenseMenuButton("Map");
        this.addObject(mapChooser);
        mapChooser.setLayoutX(450);
        mapChooser.setLayoutY(700);
        mapChooser.setOnMouseClicked(new MapChooserListener(facade.getGame(), this));
        //mapChooser.setOnAction(actionEvent -> gameStage.setScene(gameScene2));
    }

    private void createInfoLabelTower() {
        towerInfoLabel = new TowerDefenseGameLabel("You'll find \n informations \n about your \n purchases \n Right HERE");
        towerInfoLabel.setLayoutX(10);
        towerInfoLabel.setLayoutY(634);
        this.addObject(towerInfoLabel);
    }

    private void createPlayerInfoLabel2() {
        playerInfoLabel = new PlayerInfoLabel(facade.getMoneyPlayer(), facade.getScorePlayer());
        playerInfoLabel.setLayoutX(10);
        playerInfoLabel.setLayoutY(10);
        this.addObject(playerInfoLabel);
    }

    public void addObject(Node image) {//Key synchro
        synchronized (keyChildren) {
            List<Node> children = gamePane1.getChildren();
            if (!children.contains(image)) {
                children.add(image);
            }
        }
    }

    public void removeObject(Node image) {
        synchronized (keyChildren) {
            List<Node> children = gamePane1.getChildren();
            if (children.contains(image)) {
                children.remove(image);
            }
        }
    }

    void createCells() {
        Map map = this.facade.getCurrentMap();
        List<Cell> cells = map.getMyCells();
        cells.forEach(cell -> this.addObject(cell.getImageView()));
    }

    void removeCells() {
        Map map = this.facade.getCurrentMap();
        List<Cell> cells = map.getMyCells();
        cells.forEach(cell -> this.removeObject(cell.getImageView()));
        cells.clear();
    }

    void removeTowers() {
        this.facade.getTowers()
                .forEach(tower -> {
                    this.removeTower(tower);
                });
        this.facade.getTowers().clear();
    }

    void removePnjs() {
        this.facade.getPnjs()
                .forEach(pnj -> {
                    this.removeObject(pnj.getImageOfTheActor());
                });
        this.facade.getPnjs().clear();
    }

    void removeProjectils(List<Projectil> toDeleteProj) {
        toDeleteProj.forEach(proj -> {
            this.removeObject(proj.getImageOfTheActor());
        });
        synchronized (Game.keyProjectil) {
            facade.getProjectils().removeAll(toDeleteProj);
        }
    }

    private synchronized void removeTower(Tower tower) {
        this.removeObject(tower.getImageOfTheActor());
        this.removeObject(tower.getImageOfBackground());
        tower.stop();
    }

    //GETTER-SETTER
    public boolean isTower1Placable() {
        return isTower1Placable;
    }

    public boolean isTower2Placable() {
        return isTower2Placable;
    }

    public void setTower1Placable(boolean towerPlacable) {
        isTower1Placable = towerPlacable;
    }

    public void setTower2Placable(boolean tower2Placable) {
        isTower2Placable = tower2Placable;
    }

    public void setClickOnTowerUpgrade(boolean clickOnTowerUpgrade) {
        this.clickOnTowerUpgrade = clickOnTowerUpgrade;
    }

    public boolean isClickOnTowerUpgrade() {
        return clickOnTowerUpgrade;
    }

    public void setClickOnSellTower(boolean clickOnSellTower) {
        this.clickOnSellTower = clickOnSellTower;
    }

    public boolean isClickOnSellTower() {
        return clickOnSellTower;
    }

    public AnchorPane getGamePane1() {
        return gamePane1;
    }

    public TowerDefenseGameLabel getTowerInfoLabel() {
        return towerInfoLabel;
    }

    boolean isAtEndOfMap(Actors pnj) {
        return facade.isPnjAtEndOfMap(pnj);
    }

    public Tower createNewClassicTower(double x, double y) {
        return this.facade.createClassicTower(x, y);
    }

    public Tower createNewFreezTower(double x, double y) {
        return this.facade.createFreezTower(x, y);
    }

    public void upgradeTower(Tower tower) {
        this.facade.upgradeTower(tower);
    }

    public void sellTower(Tower tower) {
        tower.setOnMap(false);
        removeTower(tower);
        this.facade.getTowers().remove(tower);
    }

    public int getMoneyPlayer() {
        return this.facade.getMoneyPlayer();
    }

    public int getUpgradeTowerPrice(Tower tower) {
        return this.facade.getUpgradeTowerPrice(tower);
    }

    void setScorePlayer(int scorePlayer) {
        this.playerInfoLabel.setScore(facade.getScorePlayer());
    }

    void setMoneyPlayer(int moneyPlayer) {
        this.playerInfoLabel.setMoney(facade.getMoneyPlayer());

    }

}
