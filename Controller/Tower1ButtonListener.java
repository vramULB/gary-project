package Controller;

import Model.Game;
import Model.actors.TowerType1;
import Model.dto.TowerInfoDTO;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class Tower1ButtonListener implements EventHandler<MouseEvent> {
    private GameViewManager gameViewManager;

    public Tower1ButtonListener(GameViewManager gameViewManager) {

        this.gameViewManager = gameViewManager;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (gameViewManager.getMoneyPlayer() >= 100) {
            gameViewManager.setTower1Placable(true);
        }
    }
}
