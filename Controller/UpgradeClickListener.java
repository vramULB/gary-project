package Controller;

import Model.Facade;
import Model.Game;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class UpgradeClickListener implements EventHandler<MouseEvent> {
    
	private Facade facade;
    private GameViewManager gameViewManager;

    public UpgradeClickListener(Facade facade, GameViewManager gameViewManager) {
        this.facade = facade;
        this.gameViewManager = gameViewManager;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        //if(gameViewManager.getMoneyPlayer() >= gameViewManager.getUpgradeTowerPrice()) {
            gameViewManager.setClickOnTowerUpgrade(true);
        }
    //}
}
