package Controller;

import Model.Facade;
import Model.actors.Tower;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class PaneListener implements EventHandler<MouseEvent> {


    private GameViewManager gameViewManager;
    private int[][] map;
    private Facade facade;
    private ImageView imageView;

    public PaneListener(GameViewManager gameViewManager, int[][] map, Facade facade) {
        this.gameViewManager = gameViewManager;
        this.map = map;
        this.facade = facade;
    }


    public void handle(MouseEvent mouseEvent) {
        double x = mouseEvent.getX();
        double y = mouseEvent.getY();
        int listX = (int) x / 64;
        int listY = (int) y / 64;
        if (gameViewManager.isTower1Placable()) {
            if (map[listY][listX] != 1) {
                Tower tower = gameViewManager.createNewClassicTower(x - 30, y - 30);
                gameViewManager.addObject(tower.getImageOfBackground());
                gameViewManager.setTower1Placable(false);
            }
        } else if (gameViewManager.isTower2Placable()) {
            if (map[listY][listX] != 1) {
                Tower tower = gameViewManager.createNewFreezTower(x - 30, y - 30);
                gameViewManager.addObject(tower.getImageOfBackground());
                gameViewManager.setTower2Placable(false);
            }
        } else if (gameViewManager.isClickOnTowerUpgrade()) {
            for (Tower tower : facade.getTowers()) {
                if (Math.sqrt(Math.pow(x - tower.getX() - 30, 2) + Math.pow(y - tower.getY() - 30, 2)) < 20
                        && gameViewManager.getMoneyPlayer() >= gameViewManager.getUpgradeTowerPrice(tower)) {
                    gameViewManager.upgradeTower(tower);
                    gameViewManager.setClickOnTowerUpgrade(false);
                } else if (gameViewManager.getMoneyPlayer() < gameViewManager.getUpgradeTowerPrice(tower)) {
                    gameViewManager.setClickOnTowerUpgrade(false);
                }
            }
        } else if (gameViewManager.isClickOnSellTower()) {
            Tower towerToSell = null;
            for (Tower tower : facade.getTowers()) {
                if (Math.sqrt(Math.pow(x - tower.getX() - 30, 2) + Math.pow(y - tower.getY() - 30, 2)) < 20) {
                    towerToSell = tower;
                }
            }
            if (towerToSell != null) {
                gameViewManager.sellTower(towerToSell);
                gameViewManager.setClickOnSellTower(false);
            }

        }
    }
}