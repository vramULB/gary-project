package Controller;

import Model.Game;
import Model.actors.TowerType1;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class PaneMoverListener implements EventHandler<MouseEvent> {
    private GameViewManager gameViewManager;
    private ImageView imageView1 = new ImageView("Model/ressources/towerDefense_tile250.png");
    private ImageView imageView2 = new ImageView("Model/ressources/towerDefense_tile249.png");
    private ImageView imageView3 = new ImageView("Controller/ressources/UpgradePointer.png");
    private ImageView imageView4 = new ImageView("Controller/ressources/sellPointer.png");

    public PaneMoverListener(GameViewManager gameViewManager) {
        this.gameViewManager = gameViewManager;
        imageView1.setFitHeight(60);
        imageView1.setFitWidth(60);
        imageView2.setFitHeight(60);
        imageView2.setFitWidth(60);
        imageView3.setFitHeight(20);
        imageView3.setFitWidth(20);
        imageView4.setFitHeight(30);
        imageView4.setFitWidth(30);

    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (gameViewManager.isTower1Placable()) {
            gameViewManager.addObject(imageView1);
            imageView1.setLayoutX(mouseEvent.getX() - 30);
            imageView1.setLayoutY(mouseEvent.getY() - 30);
        }
        else if (!gameViewManager.isTower1Placable()){
            gameViewManager.removeObject(imageView1);
        }

        if (gameViewManager.isTower2Placable()){
            gameViewManager.addObject(imageView2);
            imageView2.setLayoutX(mouseEvent.getX() - 30);
            imageView2.setLayoutY(mouseEvent.getY() - 30);
        }

        else if (!gameViewManager.isTower2Placable()){
            gameViewManager.removeObject(imageView2);
        }
        if (gameViewManager.isClickOnTowerUpgrade()) {
            gameViewManager.addObject(imageView3);
            imageView3.setLayoutX(mouseEvent.getX() - 10);
            imageView3.setLayoutY(mouseEvent.getY() - 10);
        }
        else if (!gameViewManager.isClickOnTowerUpgrade()) {
            gameViewManager.removeObject(imageView3);

        }
        if (gameViewManager.isClickOnSellTower()){
            gameViewManager.addObject(imageView4);
            imageView4.setLayoutX(mouseEvent.getX() - 15);
            imageView4.setLayoutY(mouseEvent.getY() - 15);
        }
        else if (!gameViewManager.isClickOnSellTower()){
            gameViewManager.removeObject(imageView4);

        }
    }
}
