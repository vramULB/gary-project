package Controller;

import Model.dto.TowerInfoDTO;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class TowerInfoListener implements EventHandler<MouseEvent> {

    private GameViewManager gameViewManager;
    private TowerInfoDTO towerInfoDTO;

    public TowerInfoListener (GameViewManager gameViewManager, TowerInfoDTO towerInfoDTO) {
        this.gameViewManager = gameViewManager;
        this.towerInfoDTO = towerInfoDTO;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        gameViewManager.getTowerInfoLabel()
                .setText(towerInfoDTO.getName() + " \n  \n Price = " + towerInfoDTO.getPrice()  +
                        "\n \n Radius = " + towerInfoDTO.getRadius());
    }

}
