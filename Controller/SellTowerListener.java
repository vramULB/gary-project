package Controller;

import Model.Facade;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class SellTowerListener implements EventHandler<MouseEvent> {

    private Facade facade;
    private GameViewManager gameViewManager;

    public SellTowerListener(Facade facade, GameViewManager gameViewManager) {
        this.facade = facade;
        this.gameViewManager = gameViewManager;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        gameViewManager.setClickOnSellTower(true);

    }
}
