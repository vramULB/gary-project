package Controller;

import Model.Game;
import Model.Player;
import Model.WavePnj;
import View.GameViewManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * LETS GO Listener button
 */
public class StartWaveListener implements EventHandler<MouseEvent> {

    private Game game;
    private GameViewManager gameView;
    private Player player;
    private WavePnj wavePnj;

    public StartWaveListener(Game game, GameViewManager gameView) {
        this.game = game;
        this.gameView = gameView;
    }

    @Override
    public void handle(MouseEvent mouseEvent){
        gameView.play();
        /*
        game.getGameTimer().start();
        game.createWavePnj();

         */
    }
}
