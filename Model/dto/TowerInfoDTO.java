package Model.dto;

import java.io.Serializable;

public class TowerInfoDTO implements Serializable {

    private int price ;
    private int frequency;
    private double radius;
    private String name;
    private TypeTower type;

    public TowerInfoDTO(int price, int frequency, double radius, String name, TypeTower type) {
        this.price = price;
        this.frequency = frequency;
        this.radius = radius;
        this.name = name;
        this.type=type;
    }

    public int getPrice() {
        return price;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getRadius() {
        return radius;
    }

    public String getName() {
        return name;
    }

}
