package Model;

import Model.actors.Actors;
import Model.actors.Pnj;
import Model.actors.TowerType1;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import java.util.List;

public interface InteractionWithPnj {

    void interactWithPnj(Pnj pnj);
}
