package Model;

import View.GameViewManager;
import javafx.scene.image.ImageView;


public class Cell {
    private ImageView imageView;

    public Cell(String scr, float x, float y) {
        this.imageView = new ImageView(scr);
        imageView.setX(x);
        imageView.setY(y);
        imageView.setFitWidth(73);
        imageView.setFitHeight(68);
    }

    public ImageView getImageView() {
        return imageView;
    }
}
