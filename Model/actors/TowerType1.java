package Model.actors;

import Model.Game;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.concurrent.TimeUnit;

public class TowerType1 extends Tower implements Runnable {

    //Informations of the Tower
    public static int price_default = 100;
    public static int power_default = 20;
    public static int frequency_default = 20;
    public static int radius_default = 200;
    public static int upgrade_price = 100;
    public static String name_default = "Classic Tower";
    private static String source_ImageOfTheClassicTower = "model/ressources/towerDefense_tile250.png";
    private static String source_ImageOfBackground = "model/ressources/towerDefense_tile181.png";
    private ImageView imageOfTheClassicTower;
    private ImageView imageOfBackground;
    private Thread thread;
    private Game game;
    private int upgradeLevel;
    private int power;


    public TowerType1(double x, double y, int width, int height, Game game, int upgradeLevel) {
        super(x, y, width, height, price_default, frequency_default, radius_default, name_default);
        this.power = power_default;
        this.upgradePrice = upgrade_price;
        this.game = game;
        this.upgradeLevel = upgradeLevel;
        this.targetAngle = 0;
        this.currentAngle = 0;
        this.firstRotate = 180;
        this.upgradeLevel = 0;

        imageOfTheClassicTower = new ImageView(source_ImageOfTheClassicTower);
        imageOfBackground = new ImageView(source_ImageOfBackground);
        desingSetup(x, y, width, height, imageOfTheClassicTower);
        desingSetup(x, y, width, height, imageOfBackground);
        updatePlaceOfTheActor();
        thread = new Thread(this);
        thread.start();

    }

    @Override
    public Node getImageOfTheActor() {
        return imageOfTheClassicTower;
    }

    @Override
    public Node getImageOfBackground() {
        return imageOfBackground;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfTheClassicTower.setY(this.getY());
        imageOfTheClassicTower.setX(this.getX());
    }

    @Override
    public void upgrade() {
        upgradeLevel++;
        if (upgradeLevel <= 3) {
            this.setPrice(getPrice() + 100);
            this.setRadius(getRadius() + 50);
            this.setPower(power + 10);
            this.setFrequency(getFrequency() - 2);
            this.upgradePrice += 100;
        }
        if (upgradeLevel == 1) {
            //this.imageOfTheClassicTower.setImage(new Image("Model/ressources/towerDefense_tile205.png")); //TROUVER UNE AUTRE TOUR
            this.imageOfTheClassicTower.setFitHeight(getHeight() + 5);
            this.imageOfTheClassicTower.setFitWidth(getWidth() + 5);
            System.out.println("UP1");

        } else if (upgradeLevel == 2) {
            //this.imageOfTheClassicTower.setImage(new Image("Model/ressources/towerDefense_tile205.png")); //TROUVER UNE AUTRE TOUR
            this.imageOfTheClassicTower.setFitHeight(getHeight() + 10);
            this.imageOfTheClassicTower.setFitWidth(getWidth() + 10);
            System.out.println("UP2");
        } else if (upgradeLevel == 3) {
            //this.imageOfTheClassicTower.setImage(new Image("Model/ressources/towerDefense_tile205.png"));//TROUVER UNE AUTRE IMAGE
            this.imageOfTheClassicTower.setFitHeight(getHeight() + 15);
            this.imageOfTheClassicTower.setFitWidth(getWidth() + 15);
            System.out.println("UP3");
        }
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }


    @Override
    public void createProjectil(double speedX, double speedY) {
        ProjectilType1 projectil = new ProjectilType1(this.getX(), this.getY(), 40, 40, speedX, speedY, game, this);
        synchronized (Game.keyObjectToShow) {
            game.getObjectsInGame().add(projectil);
        }
        synchronized (Game.keyProjectil) {
            game.getTeamProjectils().add(projectil);
        }
    }


    @Override
    public void run() {
        while (isOnMap && super.noDeleted) {
            synchronized (Game.keyPnj) {
                if (game != null) {
                    findTarget(game.getTeamPnjz(), imageOfTheClassicTower);
                }
            }
            if (fire) {
                createProjectil(projectilSpeedX, projectilSpeedY);
                int a = 0;
                while (a < getFrequency()) {
                    try {
                        thread.sleep(20);
                        a++;
                        synchronized (Game.keyPnj) {
                            findTarget(game.getTeamPnjz(), imageOfTheClassicTower);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
