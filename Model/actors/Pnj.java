package Model.actors;

import Model.Direction;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Pnj extends Actors implements Movable, Runnable {

    private double speed;
    private int life;
    private Direction myChoice;
    private Direction lastChoice;
    private Random r = new Random();
    private int lastPositionX;
    private int lastPositionY;
    private boolean isAlive;

    public Pnj(double x, double y, int width, int height, double speed, int life) {
        super(x, y, width, height);
        this.speed = speed;
        this.life = life;
        this.myChoice = null;
        this.lastPositionX = (int) x;
        this.lastPositionY = (int) y;
        this.lastChoice = Direction.RIGHT;
        this.isAlive = true;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }


    @Override
    public List<Direction> getPossibilities(int[][] mapList, int listX, int listY, boolean checkLastChoice) {
        List<Direction> possibilities = new ArrayList<>();
        if ((listX + 1) < mapList[0].length
                && (mapList[listY][listX + 1] == 3
                || mapList[listY][listX + 1] == 1) /*case droite == 1 */) {
            possibilities.add(Direction.RIGHT);
        }
        if (mapList.length > (listY + 1)
                && (mapList[listY + 1][listX] == 3
                || mapList[listY + 1][listX] == 1)/*case bas == 1 */) {
            if (!(checkLastChoice && lastChoice == Direction.UP)) {
                possibilities.add(Direction.DOWN);
            }
        }
        if ((listY - 1) >= 0
                && (mapList[listY - 1][listX] == 3
                || mapList[listY - 1][listX] == 1)/*case haut == 1 */) {
            if (!(checkLastChoice && lastChoice == Direction.DOWN)) {
                possibilities.add(Direction.UP);
            }
        }
        return possibilities;
    }

    /**
     * suivre les 1 et pas les zeros
     *
     * @param mapList
     */
    @Override
    public Direction getNextDirection(int[][] mapList) {

        int listX = (int) (getX() / 64);
        int listY = (int) (getY() / 64);
        if (lastPositionX == listX && lastPositionY == listY) {
            return this.lastChoice;
        }
        List<Direction> possibilities = getPossibilities(mapList, listX, listY, true);
        if (possibilities.isEmpty()) { //Change direction if Mario is blocked
            possibilities = getPossibilities(mapList, listX, listY, false);
        }
        if (possibilities.isEmpty()) {
            return null;
        }
        myChoice = possibilities.get(r.nextInt(possibilities.size()));
        return myChoice;

    }

    @Override
    public void move(Direction direction, ImageView imageView) {
        switch (direction) {
            case UP:
                this.setY(this.getY() - this.speed);
                imageView.setRotate(-90);
                break;
            case DOWN:
                this.setY(this.getY() + this.speed);
                imageView.setRotate(90);
                break;
            case RIGHT:
                this.setX(this.getX() + this.speed);
                imageView.setRotate(0);
                break;
            case LEFT:
                this.setX(this.getX() - this.speed);
                break;
            default:
                throw new IllegalArgumentException("Bad direction");
        }
        lastChoice = direction;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }


    public boolean isAlive() {
        return this.isAlive ? isAlive = this.getLife() > 0 : this.isAlive;
    }

    boolean isOutOfMap(int[][] map) {
        return !(this.getPointX() < map[0].length
                && this.getPointX() >= 0
                && this.getPointY() >= 0
                && this.getPointY() < map.length);
    }

    @Override
    public abstract void run();

    public abstract void runThread();

}