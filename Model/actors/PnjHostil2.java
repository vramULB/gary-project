package Model.actors;

import Model.Direction;
import Model.Game;
import Model.InteractionWithPnj;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

// Ce pnj est une bombe, Si il arrive au bout du chemin, il fait perdre bcp plus de point (score) au chateau
// Si il arrive au chateau, il explose. Si par contre il se fait tuer en chemin, il explose et fait des
// dégats autour de lui (distance = radius)
// Ce pnj va donc plus vite

public class PnjHostil2 extends Pnj implements Runnable, InteractionWithPnj {

    private Thread thread;
    private ImageView imageOfThePnj;
    private int[][] map;
    private Game game;
    private int power;
    private int radius;

    public PnjHostil2(double x, double y, int width, int height, double speed, int life, int[][] map, Game game, int xDebut, int yDebut) {
        super(x, y, width, height, 2, 40);
        this.map = map;
        this.game = game;
        this.radius = 64;
        this.power = 30;
        super.setPointX(xDebut);
        super.setPointY(yDebut);

        imageOfThePnj = new ImageView("Model/ressources/bomberman.png");
        desingSetup(x, y, width, height, imageOfThePnj);
    }

    @Override
    public Node getImageOfTheActor() {
        return imageOfThePnj;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfThePnj.setY(this.getY());
        imageOfThePnj.setX(this.getX());
        this.setPointY((int)(imageOfThePnj.getY() / getHeight()));
        this.setPointX((int)(imageOfThePnj.getX() / getWidth()));
    }

    @Override
    public void runThread() {
        if (thread == null) {
            thread = new Thread(this);
            this.thread.start();
        }
    }


    @Override
    public void interactWithPnj(Pnj pnj) {
        pnj.setLife(pnj.getLife() - power);
    }

    private void explodes() {
        synchronized (game.keyPnj){
            for (Pnj p : game.getTeamPnjz()){
                if (p instanceof PnjHostil && isNotToFar(p)){
                    interactWithPnj(p);
                }
            }
        }
    }

    private boolean isNotToFar(Pnj p) {
        return Math.sqrt(Math.pow(p.getX() - this.getX(), 2) + Math.pow(p.getY() - this.getY(), 2)) < radius;
    }


    @Override
    public void run() {
        Direction nextDirection = Direction.RIGHT;
        while (true) {
            if (!((getX() % 64 > 0 && getX() % 64 < 63)
                    || (getY() % 64 > 0 && getY() % 64 < 63) )) {
                nextDirection = this.getNextDirection(map);
            }
            if (nextDirection != null) {
                move(nextDirection, imageOfThePnj);
            }
            if (this.getLife() <= 0) {
                explodes();
            }
            if (getLife() < 40){
                //this.imageOfThePnj.setImage(new Image(""));  // Mettre la même image en rouge
            }

            try {
                thread.sleep(10);
            } catch (InterruptedException ignored) {
            }
        }
    }
}