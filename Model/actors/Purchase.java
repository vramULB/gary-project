package Model.actors;

import Model.actors.Actors;

public abstract class Purchase extends Actors {
   
   protected int price;

    public Purchase(double x, double y, int width, int height, int price) {
        super(x, y, width, height);
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
	
}
