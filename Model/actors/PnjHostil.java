package Model.actors;

import Model.Direction;
import javafx.scene.Node;
import javafx.scene.image.ImageView;


public class PnjHostil extends Pnj {

    private Thread thread;
    private ImageView imageOfThePnj;
    private int[][] map;

    public PnjHostil(double x, double y, int width, int height, double speed, int life, int[][] map, int xDebut, int yDebut) {
        super(x, y, width, height, 2, 60);
        this.map = map;
        super.setPointX(xDebut);
        super.setPointY(yDebut);

        imageOfThePnj = new ImageView("Model/ressources/kart.png");
        desingSetup(x, y, width, height, imageOfThePnj);
    }


    @Override
    public Node getImageOfTheActor() {
        return imageOfThePnj;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfThePnj.setY(this.getY());
        imageOfThePnj.setX(this.getX());
        this.setPointY((int)(imageOfThePnj.getY() / getHeight()));
        this.setPointX((int)(imageOfThePnj.getX() / getWidth()));
    }

    @Override
    public void runThread() {
        if (thread == null) {
            thread = new Thread(this);
            this.thread.start();
        }
    }

    @Override
    public void run() {
        Direction nextDirection = Direction.RIGHT;
        while (isAlive()) {
            if (!((getX() % 64 > 0 && getX() % 64 < 63)
                    || (getY() % 64 > 0 && getY() % 64 < 63))) {
                nextDirection = this.getNextDirection(map);
            }
            if (nextDirection != null) {
                move(nextDirection, imageOfThePnj);
            }
            try {
                thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }
}