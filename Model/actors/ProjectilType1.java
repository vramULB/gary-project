package Model.actors;

import Model.Game;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

// Ce type de projectil diminue les points de vies des pnj


public class ProjectilType1 extends Projectil {

    private Thread thread;
    private ImageView imageOfTheProjectil;
    private TowerType1 towerType1;
    private Game game;

    public ProjectilType1(double x, double y, int width, int height, double speedX, double speedY, Game game, TowerType1 towerType1) {
        super(x, y, width, height, speedX, speedY, 1, game);
        this.towerType1 = towerType1;
        this.game = game;


        imageOfTheProjectil = new ImageView("Model/ressources/towerDefense_tile295.png");
        desingSetup(x, y, width, height, imageOfTheProjectil);


        thread = new Thread(this);
        thread.start();
    }

    @Override
    public Node getImageOfTheActor() {

        return imageOfTheProjectil;
    }


    @Override
    public void updatePlaceOfTheActor() {
        imageOfTheProjectil.setY(this.getY());
        imageOfTheProjectil.setX(this.getX());
    }

    @Override
    public boolean outOfRadius() {
        return Math.sqrt(Math.pow(towerType1.getX() - this.getX(), 2) + Math.pow(towerType1.getY() - this.getY(), 2)) >= towerType1.getRadius();
    }

    @Override
    public void interactWithPnj(Pnj pnj) {
        if (pnj instanceof PnjHostil || pnj instanceof PnjHostil2) {
            pnj.setLife(pnj.getLife() - towerType1.getPower());
        }
        else if (pnj instanceof PnjFriendly){
            pnj.setLife(pnj.getLife() + towerType1.getPower());
        }
    }



    @Override
    public void run() {
        while (!outOfRadius() && !hasTouch()) {
            super.move();
            synchronized (Game.keyPnj) {
                findTarget(game.getTeamPnjz());
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }

}
