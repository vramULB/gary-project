package Model.actors;

import Model.Game;
import Model.InteractionWithPnj;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import java.util.List;

public abstract class Projectil extends Actors implements InteractionWithPnj, Runnable {

    private Thread thread;
    private ImageView design;
    private double speedX;
    private double speedY;
    private TowerType1 towerType1;
    private Game game;
    private int life;
    private int power;

    public Projectil(double x, double y, int width, int height, double speedX, double speedY, int life, Game game) {
        super(x, y, width, height);
        this.speedX = speedX;
        this.speedY = speedY;
        this.life = life;
        this.game = game;
        this.power = power;
    }


    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    void move() {
        this.setX(this.getX() + this.speedX * 5);
        this.setY(this.getY() + this.speedY * 5);
    }

    public abstract boolean outOfRadius();

    public boolean hasTouch() {
        return life <= 0;
    }

    @Override
    public void interactWithPnj(Pnj pnj) {

        pnj.setLife(pnj.getLife() - power);
    }

    public void findTarget(List<Pnj> teamPnjz) {
        synchronized (game.keyPnj) {
                for (Pnj p : teamPnjz) {
                    if (Math.sqrt(Math.pow(this.getX() - p.getX(), 2) + Math.pow(this.getY() - p.getY(), 2)) < 20
                            && !this.outOfRadius() && p.isAlive()) {
                        this.interactWithPnj(p);
                        this.setLife(this.getLife() - 1);
                    }
                }
        }
    }

}
