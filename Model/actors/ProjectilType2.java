package Model.actors;

import Model.Game;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

// Ce type de porjectil diminue la vitesse du pnj --> Voir méthode interact

public class ProjectilType2 extends Projectil {

    private Thread thread;
    private ImageView imageOfTheProjectil;
    private TowerType2 towerType2;
    private Game game;


    public ProjectilType2(double x, double y, int width, int height, double speedX, double speedY, Game game, TowerType2 towerType2) {
        super(x, y, width, height, speedX, speedY, 1, game);
        this.towerType2 = towerType2;
        this.game = game;

        imageOfTheProjectil = new ImageView("Model/ressources/towerDefense_tile295.png");
        desingSetup(x, y, width, height, imageOfTheProjectil);


        thread = new Thread(this);
        thread.start();
    }

    @Override
    public Node getImageOfTheActor() {

        return imageOfTheProjectil;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfTheProjectil.setY(this.getY());
        imageOfTheProjectil.setX(this.getX());
    }

    @Override
    public boolean outOfRadius() {
        return Math.sqrt(Math.pow(towerType2.getX() - this.getX(), 2) + Math.pow(towerType2.getY() - this.getY(), 2)) >= towerType2.getRadius();
    }

    @Override
    public void interactWithPnj(Pnj pnj) {
        if (pnj instanceof PnjHostil || pnj instanceof PnjHostil2) {
            pnj.setSpeed(0);
            try {
                Thread.sleep(towerType2.getFreezDuration());
            } catch (InterruptedException e) {
            }
            pnj.setSpeed(2);
        }
    }

    @Override
    public void run() {
        while (true) {
            super.move();
            synchronized (Game.keyPnj) {
                findTarget(game.getTeamPnjz());
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }

}
