package Model.actors;


import Model.Direction;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

public abstract class Actors {
    
	private double x;
    private double y;
    private int width;
    private int height;
    private int pointX;
    private int pointY;

    public Actors(double x, double y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public abstract void updatePlaceOfTheActor();

    public int getPointX() {
        return pointX;
    }

    public void setPointX(int pointX) {
        this.pointX = pointX;
    }

    public int getPointY() {
        return pointY;
    }

    public void setPointY(int pointY) {
        this.pointY = pointY;
    }

    public abstract Node getImageOfTheActor();

    public void desingSetup(double x, double y, int width, int height, ImageView imageView) {
        imageView.setX(x);
        imageView.setY(y);
        imageView.setFitHeight(height);
        imageView.setFitWidth(width);
    }

}
