package Model.actors.todo;
/*
import Model.Direction;
import Model.Game;
import Model.Interaction;
import Model.actors.Movable;
import Model.actors.Pnj;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import java.util.List;
import java.util.Random;

public class TrapType1 extends Trap implements Interaction, Runnable, Movable {
    
	private Thread thread;
    private ImageView design;
    private Game game;

    public TrapType1(int x, int y, int width, int height, int price, int damage, int speed, int life, Game game) {
        super(x, y, width, height, price, 100, 3, 1);
        this.price = 50;
        this.game = game;

        design = new ImageView("model/ressources/rocket.png");
        design.setX(x);
        design.setY(y);
        design.setFitHeight(height);
        design.setFitWidth(width);

        thread = new Thread(this);
        thread.start();
    }

    public Node getTrap() {
        return design;
    }

    public void update() {
        design.setY(this.getY());
        design.setX(this.getX());
    }

    @Override
    public boolean hasTouchPnj() {
        return this.getLife() <= 0;
    }

    private void findTarget(List<Pnj> teamPnjz) {
        synchronized (game.keyPnj) {
            for (Pnj p : teamPnjz) {
                if (isPnjInApproach(p, this) && !this.hasTouchPnj() && p.isAlive()) {
                    this.interact(p);
                    this.setLife(this.getLife() - 1);
                    this.hasTouchPnj();
                    game.getPlayer().setMoney(game.getPlayer().getMoney() + 50);
                }
            }
        }
    }

    @Override
    public void interact(Pnj pnj) {
        pnj.setLife(pnj.getLife() - this.getDamage());
    }

    private boolean isPnjInApproach(Pnj pnj, TrapType1 trapType1) {
        return Math.sqrt(Math.pow(pnj.getX() - trapType1.getX(), 2) + Math.pow(pnj.getY() - trapType1.getY(), 2)) < 20;
    }

    @Override
    public void run() {
        Random r = new Random();
        Random a = new Random();
        while(true){

            move(null);
            findTarget(game.getTeamPnjz());

            try {
                thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Direction getNextDirection(int[][] mapList) {
        return null;
    }

    @Override
    public void move(Direction direction) {

    }

    @Override
    public List<Direction> getPossibilities(int[][] mapList, int listX, int listY, boolean checkLastChoice) {
        return null;
    }

}
*/
