package Model.actors;

import Model.Direction;
import Model.actors.Pnj;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

public class PnjFriendly extends Pnj implements Runnable {

    private Thread thread;
    private ImageView imageOfThePnj;
    private int[][] map;
    private int itsTimeToLoseLife;

    public PnjFriendly(double x, double y, int width, int height, double speed, int life, int[][] map) {
        super(x, y, width, height, 2, 60);
        this.map = map;
        this.itsTimeToLoseLife = 25;

        imageOfThePnj = new ImageView("Model/ressources/towerDefense_tile271.png");
        desingSetup(x, y, width, height, imageOfThePnj);
    }

    @Override
    public Node getImageOfTheActor() {
        return imageOfThePnj;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfThePnj.setY(this.getY());
        imageOfThePnj.setX(this.getX());
        this.setPointY((int) (imageOfThePnj.getY() / getHeight()));
        this.setPointX((int) (imageOfThePnj.getX() / getWidth()));
    }

    @Override
    public void runThread() {
        if (thread == null) {
            thread = new Thread(this);
            this.thread.start();
        }
    }

    private void loseLife() {
        this.setLife(this.getLife() - 3);
    }

    @Override
    public void run() {
        Direction nextDirection = Direction.RIGHT;
        while (isAlive()) {
            loseLife();
            int a = 0;
            while (a < itsTimeToLoseLife) {
                try {
                    thread.sleep(10);
                    a++;
                    if (!((getX() % 64 > 0 && getX() % 64 < 63)
                            || (getY() % 64 > 0 && getY() % 64 < 63))) {
                        nextDirection = this.getNextDirection(map);
                    }
                    if (nextDirection != null) {
                        move(nextDirection, imageOfThePnj);
                    }
                } catch (InterruptedException e) {
                }
            }
            try {
                thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }
}
