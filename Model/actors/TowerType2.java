package Model.actors;

import Model.Game;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class TowerType2 extends Tower implements Runnable {

    //Informations of the Tower
    public static int price_default = 200;
    public static int FreezDuration_default = 10;
    public static int frequency_default = 100;
    public static int radius_default = 200;
    public static int upgrade_price = 200;
    public static String name_default = "Freez Tower";

    private static String source_ImageOfTheFreezTower = "model/ressources/towerDefense_tile249.png";
    private static String source_ImageOfBackground = "model/ressources/towerDefense_tile181.png";
    private ImageView imageOfTheFreezTower;
    private ImageView imageOfBackground;
    private Thread thread;
    private Game game;
    private int upgradeLevel;
    private int freezDuration;


    public TowerType2(double x, double y, int width, int height, Game game, int upgradeLevel) {
        super(x, y, width, height, price_default, frequency_default, radius_default, name_default);
        this.game = game;
        this.upgradeLevel = upgradeLevel;
        this.targetAngle = 0;
        this.currentAngle = 0;
        this.firstRotate = 180;
        this.upgradeLevel = 0;
        this.freezDuration = 150;
        this.upgradePrice = upgrade_price;

        imageOfTheFreezTower = new ImageView(source_ImageOfTheFreezTower);
        imageOfBackground = new ImageView(source_ImageOfBackground);
        desingSetup(x, y, width, height, imageOfTheFreezTower);
        desingSetup(x, y, width, height, imageOfBackground);

        updatePlaceOfTheActor();

        thread = new Thread(this);
        thread.start();

    }

    @Override
    public Node getImageOfTheActor() {
        return this.imageOfTheFreezTower;
    }

    @Override
    public Node getImageOfBackground() {
        return this.imageOfBackground;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfTheFreezTower.setY(this.getY());
        imageOfTheFreezTower.setX(this.getX());
    }

    @Override
    public void upgrade() {
        upgradeLevel++;
        if (upgradeLevel <= 3) {
            this.setPrice(getPrice() + 100);
            this.setRadius(getRadius() + 50);
            this.setFreezDuration(freezDuration + 3);
            this.setFrequency(getFrequency() - 3);
            this.upgradePrice += 200;
        }
        if (upgradeLevel == 1) {
            //this.imageOfTheFreezTower.setImage(new Image("...")); //TROUVER UNE AUTRE TOUR
            this.imageOfTheFreezTower.setFitHeight(getHeight() + 5);
            this.imageOfTheFreezTower.setFitWidth(getWidth() + 5);
        } else if (upgradeLevel == 2) {
            //this.imageOfTheFreezTower.setImage(new Image("...")); //TROUVER UNE AUTRE TOUR
            this.imageOfTheFreezTower.setFitHeight(getHeight() + 10);
            this.imageOfTheFreezTower.setFitWidth(getWidth() + 10);
        } else if (upgradeLevel == 3) {
            //this.imageOfTheFreezTower.setImage(new Image("..."));//TROUVER UNE AUTRE IMAGE
            this.imageOfTheFreezTower.setFitHeight(getHeight() + 15);
            this.imageOfTheFreezTower.setFitWidth(getWidth() + 15);
        }
    }

    public void setFreezDuration(int freezDuration) {
        this.freezDuration = freezDuration;
    }

    public int getFreezDuration() {
        return freezDuration;
    }


    @Override
    public void createProjectil(double speedX, double speedY) {
        ProjectilType2 projectil = new ProjectilType2(this.getX(), this.getY(), 40, 40, speedX, speedY, game, this);
        synchronized (game.keyObjectToShow) {
            game.getObjectsInGame().add(projectil);
        }
        synchronized (game.keyProjectil) {
            game.getTeamProjectils().add(projectil);
        }
    }


    @Override
    public void run() {
        while (isOnMap && noDeleted) {
            synchronized (Game.keyPnj) {
                findTarget(game.getTeamPnjz(), imageOfTheFreezTower);
            }
            if (fire) {
                createProjectil(projectilSpeedX, projectilSpeedY);
                int a = 0;
                while (a < getFrequency()) {
                    try {
                        thread.sleep(20);
                        a++;
                        synchronized (Game.keyPnj) {
                            findTarget(game.getTeamPnjz(), imageOfTheFreezTower);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
