package Model.actors;

import Model.Direction;
import javafx.scene.image.ImageView;

import java.util.List;

public interface Movable {
    Direction getNextDirection(int[][] mapList);
    void move(Direction direction, ImageView imageView);
    List<Direction> getPossibilities(int[][] mapList, int listX, int listY, boolean checkLastChoice);
}
