package Model.actors;

import Model.Direction;
import Model.Game;
import Model.InteractionWithPnj;
import Model.actors.Pnj;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

public class PnjFriendly2 extends Pnj implements Runnable, InteractionWithPnj {

    private Thread thread;
    private ImageView imageOfThePnj;
    private int[][] map;
    private Game game;
    private int radius;
    private int saveCapability;
    private boolean isInApproach;

    public PnjFriendly2(double x, double y, int width, int height, double speed, int life, int[][] map, Game game) {
        super(x, y, width, height, 2, 1);
        this.map = map;
        this.game = game;
        this.radius = 200;
        this.saveCapability = 5;

        imageOfThePnj = new ImageView("Model/ressources/towerDefense_tile270.png");
        imageOfThePnj.setRotate(25);
        desingSetup(x, y, width, height, imageOfThePnj);
    }

    @Override
    public Node getImageOfTheActor() {
        return imageOfThePnj;
    }

    @Override
    public void updatePlaceOfTheActor() {
        imageOfThePnj.setY(this.getY());
        imageOfThePnj.setX(this.getX());
        this.setPointY((int)(imageOfThePnj.getY() / getHeight()));
        this.setPointX((int)(imageOfThePnj.getX() / getWidth()));
    }


    @Override
    public void runThread() {
        if (thread == null) {
            thread = new Thread(this);
            this.thread.start();
        }
    }

    public void move(){
        this.setX(getX() + getSpeed()+1);
        this.setY(getY() + getSpeed() - 1);

    }

    @Override
    public void run() {
        while (isAlive()) {
            move();
            for (Pnj pnj : game.getTeamPnjz()){
                if (isPnjInApproach(pnj)) {
                    interactWithPnj(pnj);
            }
            }
            try {
                thread.sleep(10);
            } catch (InterruptedException ignored) {
            }
        }
    }

    private boolean isPnjInApproach(Pnj pnj) {
        if (pnj instanceof PnjFriendly) {
            isInApproach = Math.sqrt(Math.pow(pnj.getX() - this.getX(), 2) + Math.pow(pnj.getY() - this.getY(), 2)) < radius;
        }
        return isInApproach;
    }

    @Override
    public void interactWithPnj(Pnj pnj) {
        System.out.println("I saved him");
        if (pnj instanceof PnjFriendly) {
            pnj.setLife(pnj.getLife() + saveCapability);
        }
    }
}
