package Model.actors;

import Model.Game;
import Model.dto.TowerInfoDTO;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import java.util.List;


public abstract class Tower extends Purchase {

    private String name;
    private int frequency;
    private double radius;
    private Pnj target;
    protected double firstRotate;
    protected double targetAngle;
    protected double currentAngle;
    protected boolean fire;
    protected double projectilSpeedX;
    protected double projectilSpeedY;
    protected boolean isOnMap;
    protected int upgradePrice;
    protected boolean noDeleted;

    public Tower(double x, double y, int width, int height, int price, int frequency, double radius, String name) {
        super(x, y, width, height, price);
        this.frequency = frequency;
        this.radius = radius;
        this.name = name;
        this.isOnMap = true;
        noDeleted = true;
    }

    public int getUpgradePrice() {
        return upgradePrice;
    }


    public void setOnMap(boolean isOnMap) {
        this.isOnMap = isOnMap;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public abstract void upgrade();

    public abstract void createProjectil(double targetX, double targetY) throws InterruptedException;

    public Pnj getTarget() {
        return target;
    }

    public void setTarget(Pnj target) {
        this.target = target;
    }

    public boolean isPnjThere(Pnj pnj, Tower tower) {
        return Math.sqrt(Math.pow(pnj.getX() - tower.getX(), 2) + Math.pow(pnj.getY() - tower.getY(), 2)) < tower.getRadius();
    }

    public void findTarget(List<Pnj> teamPnjz, ImageView imageView) {

        if (getTarget() != null && target.isAlive() && isPnjThere(target, this)) {
            turnOver(target, imageView);
        } else {
            target = null;
            fire = false;
        }

        Pnj closestTarget = null;
        double closestDistance = 0.0;

        synchronized (Game.keyPnj) {
            for (Pnj target : teamPnjz) {
                if (!target.isAlive()) {
                    continue;
                }

                double distanceX = target.getX() - this.getX();
                double distanceY = target.getY() - this.getY();

                double distanceTotal = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

                if (Double.compare(distanceTotal, this.getRadius()) > 0) {
                    continue;
                }

                if (closestTarget == null || Double.compare(distanceTotal, closestDistance) < 0) {
                    closestTarget = target;
                    closestDistance = distanceTotal;
                }
            }
        }
        setTarget(closestTarget);
    }


    public void turnOver(Pnj target, ImageView imageView) {
        fire = false;

        if (target != null) {
            double xDist = target.getX() - this.getX();
            double yDist = target.getY() - this.getY();

            double angleToTarget = Math.atan2(yDist, xDist) - Math.PI / 2;

            double targetAngle = Math.toDegrees(angleToTarget);
            double currentAngle = this.firstRotate;

            if (Math.abs(currentAngle) > 360) {
                if (currentAngle < 0) {
                    currentAngle = currentAngle % 360 + 360;
                } else {
                    currentAngle = currentAngle % 360;
                }
            }
            double diff = targetAngle - currentAngle;

            if (Math.abs(diff) < 180) {

            } else {
                if (diff > 0) {
                    targetAngle -= 360;
                } else {
                    targetAngle += 360;
                }
            }
            diff = targetAngle - currentAngle;
            currentAngle += diff + 180;


            imageView.setRotate(currentAngle);

            double distance = Math.sqrt(Math.pow(target.getX() - this.getX(), 2) + Math.pow(target.getY() - this.getY(), 2));
            projectilSpeedX = (target.getX() - this.getX()) / distance;
            projectilSpeedY = (target.getY() - this.getY()) / distance;
            fire = true;

        }
    }

    public void stop() {
        noDeleted = false;
    }


    public abstract Node getImageOfBackground();
}

