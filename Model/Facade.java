package Model;

import Model.actors.*;
import Model.dto.TowerInfoDTO;

import java.util.List;

public interface Facade {

    void createGame(Player player, int nbMap);

    void createWave(int nbPnj, int level);

    List<Pnj> getPnjs();

    int getScorePlayer();

    int getMoneyPlayer();

    boolean isPnjAtEndOfMap(Actors pnj);

    List<Tower> getTowers();

    Map getCurrentMap();

    TowerInfoDTO getInfoTowerType1();
    TowerInfoDTO getInfoTowerType2();

    Game getGame();

    TowerType1 createClassicTower(double x, double y);

    TowerType2 createFreezTower(double x, double y);

    List<Projectil> getProjectils();

    void stopCurrentWave();

    void upgradeTower(Tower tower);

    void modifyScorePlayer(Pnj pnj);

    int getUpgradeTowerPrice(Tower tower);

    void modifyMoneyPnjPlayer(Pnj pnj);

    int[][] getCurrentMapArray();

    boolean isEndOfGame();

    void nextMap();
}
