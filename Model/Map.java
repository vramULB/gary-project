package Model;

import View.GameViewManager;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Map {
    private final int NB_OUT = 0;
    private final int NB_WAY = 1;
    private final int NB_FIRST = 2;
    private final int NB_END = 3;
    private int pointXStart;
    private int pointYStart;
    private int pointXEnd;
    private int pointYEnd;

    private int[][] mapArray;
    private List<Cell> myCells;

    public Map(int[][] mapArray, String srcImageWay, String srcImageOutsideWay1) {
        this.mapArray = mapArray;
        this.myCells = new ArrayList<>();
        createMap(srcImageWay, srcImageOutsideWay1);
    }

    public int[][] getMapArray() {
        return mapArray;
    }

    public void createMap(String srcImageWay, String srcImageOutsideWay) {
        Cell cell;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 20; j++) {
                String img = "Model/ressources/";
                switch (mapArray[i][j]) {
                    case NB_OUT:
                        img += srcImageOutsideWay;
                        break;
                    case NB_WAY:
                        img += srcImageWay;
                        break;
                    case NB_FIRST:
                        this.pointXStart = j;
                        this.pointYStart = i;
                        img += srcImageWay;
                        break;
                    case NB_END:
                        this.pointXStart = j;
                        this.pointYStart = i;
                        img += srcImageWay;
                        break;

                }
                myCells.add(new Cell(img, j * 64, i * 64));
            }
        }
    }

    public List<Cell> getMyCells() {
        return myCells;
    }

    public int getPointXStart() {
        return pointXStart;
    }

    public int getPointYStart() {
        return pointYStart;
    }

    public int getPointXEnd() {
        return pointXEnd;
    }

    public int getPointYEnd() {
        return pointYEnd;
    }
}