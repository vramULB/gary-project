package Model;

import Model.actors.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WavePnj implements Runnable {

    private int level;
    private int numberPnj;
    private int life;
    private double speed;
    private List<Pnj> listPnj;
    private Thread thread;
    private int iPnj;
    private int[][] mapList;
    public static Object keyPnj = new Object();
    private Game game;
    private int maxNumberPnjFriendly2ByWave;
    private int currentNumberPnjFriendly2InTheWave;


    public WavePnj(int level, int numberPnj, int[][] mapList, Game game) {
        this.level = level;
        this.numberPnj = numberPnj;
        this.mapList = mapList;
        this.game = game;
        this.maxNumberPnjFriendly2ByWave = 1;
        this.currentNumberPnjFriendly2InTheWave = 0;
        //List Pnj
        listPnj = new ArrayList<>();
        thread = new Thread(this);
        //CreatePnj
        iPnj = 1;
        createPnj();
        thread.start();
    }

    public void createPnj() {
        Random r = new Random();
        int a;
        a = r.nextInt(4);
        if (a == 0 || a == 1 || a == 2) {
            /*PnjHostil pnjH = new PnjHostil(0, 448, 64, 64, speed, life, mapList, 0, 0);
            //synchronized (keyPnj) {
            this.listPnj.add(pnjH);*/
            PnjFriendly pnjF = new PnjFriendly(0, 448, 64, 64, speed, life, mapList);
            this.listPnj.add(pnjF);
        }
        else if (a == 3 && currentNumberPnjFriendly2InTheWave < maxNumberPnjFriendly2ByWave){
           // PnjHostil2 pnjH = new PnjHostil2(0, 448, 64, 64, speed, life, mapList, game, 0, 0);
           // this.listPnj.add(pnjH);
            PnjFriendly2 pnjF = new PnjFriendly2(0, 0, 64, 64, speed, life, mapList, game);
            this.listPnj.add(pnjF);
            currentNumberPnjFriendly2InTheWave++;
        }
        //}
    }

    public List<Pnj> getListPnj() {
        synchronized (game.keyPnj) {
            return listPnj;
        }
    }

    @Override
    public void run() {
        while (!listPnj.isEmpty()) {
            if (iPnj < numberPnj) {
                createPnj();
                //start Pnj au lieu de create
                iPnj++;
                //Wait to stop next PNJ
                try {
                    this.thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

        }
    }

    public void interrupt() {
        this.thread.interrupt();
    }

    public Object getKeyListPnj() {
        return this.keyPnj;
    }
}