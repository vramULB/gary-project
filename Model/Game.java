package Model;

import Model.actors.*;
import Model.dto.TowerInfoDTO;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private Player player;
    public static final Object keyPnj = new Object();
    public static Object keyProjectil = new Object();
    public static Object keyObjectToShow = new Object();
    private int[][] mapList;

    private List<Tower> teamTowerz;
    private List<Actors> objectsInGame;
    private List<Projectil> teamProjectils;
    private WavePnj currentWave;
    private boolean endOfGame;
    //private TowerInfoDTO tower1 = new TowerInfoDTO();

    public Game(Player player, int[][] mapList) {
        this.player = player;
        this.mapList = mapList;
        this.teamTowerz = new ArrayList<>();
        this.objectsInGame = new ArrayList<>();
        this.teamProjectils = new ArrayList<>();
        this.endOfGame=false;
    }


    //PART 1 : PNJ + WAVE

    //CREATION OF THE WAVE
    public void createWavePnj(int nbPnj, int level) {
        currentWave = new WavePnj(level, nbPnj, mapList, this);
    }

    //CREATION OF THE PNJ'S

    // ACCESS LIST PNJ'S
    public List<Pnj> getTeamPnjz() {
        if (this.currentWave == null) {
            return new ArrayList<>();
        }
        return this.currentWave.getListPnj();
    }

    //PART 2 : TOWER

    // CREATION OF THE TOWERS
    public TowerType1 createNewClassicTower(double x, double y) {
        TowerType1 tower = new TowerType1(x, y, 60, 60, this, 0);
        teamTowerz.add(tower);
        this.getPlayer().modifyMoney(tower);
        return tower;
    }


    public TowerType2 createNewFreezTower(double x, double y) {
        TowerType2 tower = new TowerType2(x, y, 60, 60, this, 0);
        teamTowerz.add(tower);
        this.getPlayer().modifyMoney(tower);
        return tower;
    }

    // ACCESS LIST TOWERS
    public List<Tower> getTeamTower() {
        return teamTowerz;
    }

    public synchronized List<Projectil> getTeamProjectils() {
        return teamProjectils;
    }

    public List<Actors> getObjectsInGame() {
        return objectsInGame;
    }


    public Player getPlayer() {
        return player;
    }


    public void upgradeTower(Tower tower) {
        tower.upgrade();
        this.getPlayer().modifyUpgradeMoney(tower);
        }


    public void stopCurrentWave() {
        if (currentWave != null) {
            currentWave.interrupt();
        }
    }

    public void setMapList(int[][] mapList) {
        this.mapList = mapList;
    }

    public boolean isEndOfGame() {
        return endOfGame;
    }

    public void setEndOfGame(boolean endOfGame) {
        this.endOfGame = endOfGame;
    }
}
