package Model;

import Model.actors.*;
import Model.dto.TowerInfoDTO;
import Model.dto.TypeTower;

import java.util.List;

public class FacadeImpl implements Facade {
    private final String map1ImageScrOut1 = "towerDefense_tile098.png";
    private final String map1ImageScrWay = "towerDefense_tile257.png";
    private final String map1ImageScrStart = "";
    private final String map1ImageScrEnd = "";

    private final String map2ImageScrWay = "towerDefense_tile034.png";
    private final String map2ImageScrOut = "towerDefense_tile055.png";
    private final String map2ImageScrStart = "";
    private final String map2ImageScrEnd = "";
    private Game game;
    private Map currentMapObj;
    private int indexMap;
    private static int maxIndexMap = 2;

    @Override
    public void createGame(Player player, int indexMap) {
        this.indexMap = indexMap;
        this.currentMapObj = new Map(getMap1(), map1ImageScrWay, map1ImageScrOut1);
        this.game = new Game(player, currentMapObj.getMapArray());

    }

    @Override
    public void createWave(int nbPnj, int level) {
        this.game.createWavePnj(nbPnj, level);
    }

    @Override
    public List<Pnj> getPnjs() {
        return this.game.getTeamPnjz();

    }

    @Override
    public int getScorePlayer() {
        return this.game.getPlayer().getScore();
    }

    @Override
    public int getMoneyPlayer() {
        return this.game.getPlayer().getMoney();
    }


    private int[][] getMap2() {
        return new int[][]{
                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3},
                {1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
    }

    private int[][] getMapEnd() {
        return new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0},
                {0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0},
                {0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
    }

    private int[][] getMap1() {
        return new int[][]{
                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0},
                {0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 3},
                {1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0}
        };
    }

    @Override
    public boolean isPnjAtEndOfMap(Actors pnj) {
        return this.currentMapObj.getMapArray()[pnj.getPointY()][pnj.getPointX()] == 3;
    }


    @Override
    public List<Tower> getTowers() {
        return this.game.getTeamTower();
    }

    @Override
    public Map getCurrentMap() {
        return this.currentMapObj;
    }

    @Override
    public int[][] getCurrentMapArray() {
        return this.currentMapObj.getMapArray();
    }

    @Override
    public TowerInfoDTO getInfoTowerType1() {
        return new TowerInfoDTO(TowerType1.price_default, TowerType1.frequency_default, TowerType1.radius_default, TowerType1.name_default, TypeTower.TYPE_1);
    }

    @Override
    public TowerInfoDTO getInfoTowerType2() {
        return new TowerInfoDTO(TowerType2.price_default, TowerType2.frequency_default, TowerType2.radius_default, TowerType2.name_default, TypeTower.TYPE_2);
    }

    @Override
    public Game getGame() {
        return game;
    }

    @Override
    public TowerType1 createClassicTower(double x, double y) {
        return this.game.createNewClassicTower(x, y);
    }

    @Override
    public TowerType2 createFreezTower(double x, double y) {
        return this.game.createNewFreezTower(x, y);
    }

    @Override
    public List<Projectil> getProjectils() {
        return this.game.getTeamProjectils();
    }

    @Override
    public void stopCurrentWave() {
        game.stopCurrentWave();
    }

    @Override
    public boolean isEndOfGame() {
        return game.isEndOfGame();
    }


    @Override
    public void nextMap() {
        ++indexMap;
        switch (indexMap) {
            case 1:
                this.currentMapObj = new Map(getMap1(), map1ImageScrWay, map1ImageScrOut1);
                break;
            case 2:
                this.currentMapObj = new Map(getMap2(), map1ImageScrWay, map1ImageScrOut1);
                break;
            default:
                game.setEndOfGame(true);
                this.currentMapObj = new Map(getMapEnd(), map1ImageScrWay, map1ImageScrOut1);
        }
        this.game.setMapList(currentMapObj.getMapArray());
    }

    @Override
    public void upgradeTower(Tower tower) {
        game.upgradeTower(tower);
    }

    @Override
    public void modifyScorePlayer(Pnj pnj) {
        this.game.getPlayer().modifyScore(pnj);
    }

    @Override
    public int getUpgradeTowerPrice(Tower tower) {
        return tower.getUpgradePrice();
    }

    @Override
    public void modifyMoneyPnjPlayer(Pnj pnj) {
        this.game.getPlayer().modifyPnjMoney(pnj);
    }
}
