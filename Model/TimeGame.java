package Model;

public class TimeGame {

    private long lastTime;
    private long currentTime;
    private long timeStartWave;
    private long dt;


    public TimeGame(long dt) {
        this.lastTime = System.currentTimeMillis();
        this.timeStartWave = System.currentTimeMillis();
        this.dt = dt;

    }

    public void update() {
        currentTime = System.currentTimeMillis();
        dt += currentTime - lastTime;
        lastTime = currentTime;
    }

    public long getDt() {
        return dt / 250;
    }

    public boolean isTimeWavePass() {
        currentTime = System.currentTimeMillis();
        long timePassed = currentTime - lastTime;
        if (timePassed > 10000) {
            return true;
        }
        return false;
    }

    public boolean isPassed200milSec() {
        long curTime = System.currentTimeMillis();
        long timePassed = curTime - timeStartWave;
        if(timePassed > 200){
            timeStartWave = curTime;
            return true;
        }
        return false;

    }

    public void setTimeStartWave(long timeStartWave) {
        this.timeStartWave = timeStartWave;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void reset() {
        currentTime = System.currentTimeMillis();
        lastTime = System.currentTimeMillis();
        timeStartWave = System.currentTimeMillis();
    }

}
