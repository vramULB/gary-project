package Model;

import Model.actors.*;

public class Player {

    private int money;
    private String name;
    private int score;
    //private final int SCORE_PNJHOSTIL = 50;
    private final int SCORE_PNJBOMBE = 100;
    private final int SCORE_PNJFRIENDLY = 100;
    private final int KILL_PNJHOSTIL = 20;
    private final int KILL_PNJBOMBE = 30;
    private final int END_PNJFRIENDLY = 30;

    public Player(String name, int money, int score) {
        this.money = money;
        this.name = name;
        this.score = score;
    }

    public int getMoney() {

        return money;
    }

    public void setMoney(int money) {

        this.money = money;
    }

    public void addMoney(int money) {
        this.money += money;
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void modifyScore(Pnj pnj) {
        int SCORE_PNJHOSTIL = 50;
        if (pnj instanceof PnjHostil){
            score = score - SCORE_PNJHOSTIL;
        }
        else if (pnj instanceof PnjHostil2){
            score = score - SCORE_PNJBOMBE;
        }
        else if (pnj instanceof PnjFriendly){
            score = score + SCORE_PNJFRIENDLY;
            money = money + END_PNJFRIENDLY;
        }
    }

    public void modifyMoney(Tower tower){

        money = money - tower.getPrice();
        }

    public void modifyUpgradeMoney(Tower tower) {
        money = money - tower.getUpgradePrice();
    }

    public void modifyPnjMoney(Pnj pnj) {

        if (pnj instanceof PnjHostil){
            money = money + KILL_PNJHOSTIL;
        }
        else if (pnj instanceof PnjHostil2){
            money = money + KILL_PNJBOMBE;
        }
    }
}
