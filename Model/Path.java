package Model;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Path {
	
    private Node border;

    public Path (double posX, double posY, double width, double height) {
        this.border = new Rectangle(width, height, Color.TRANSPARENT);
        border.setTranslateX(posX);
        border.setTranslateY(posY);
    }
    public Node getView() {
        return border;
    }

}
